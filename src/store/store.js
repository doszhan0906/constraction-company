import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		tasks: [
			{
				id: 1,
				title: 'Task 1',
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
				date: new Date()
			},
			{
				id: 2,
				title: 'Task 2',
				date: new Date(),
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`
			},
			{
				id: 3,
				title: 'Task 3',
				date: new Date(),
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
				status: 'completed'
			},
			{
				id: 4,
				title: 'Task 4',
				date: new Date(),
				description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
				status: 'completed'
			}
		],
		users: [],
		currentUser: null
	},
	mutations: {
		registerUser(state, data) {
			state.users.push(data);
		},
		authorizeModalShow(state) {
			state.authotizingUser = true
		},
		authorizeUser(state, data) {
			state.currentUser  = data;
		},
		addNewTask(state, data) {
			state.tasks.push(data)
		},
		editTask (state, taskData) {
			state.tasks[taskData.index] = {...state.tasks[taskData.index], ...taskData.newData}
			state.tasks = [...state.tasks]
		},
		completeTask (state, index) {
			state.tasks[index] = {...state.tasks[index], ...{status: 'completed'}}
			state.tasks = [...state.tasks]
		},
		deleteTask (state, index) {
			state.tasks.splice(index,1)
		}
	},
	actions: {
		addTask (context, newTaskData) {
			let maxId = 0
			context.state.tasks.forEach(task=>{
				if(task.maxId > maxId) maxId = task.maxId
			})
			newTaskData.id = maxId;
			context.commit('addNewTask', newTaskData)
		},
		editTask (context, taskData) {
			for(let i in context.state.tasks) {
				if(taskData.task_id === context.state.tasks[i].id) taskData.index = i
			}
			context.commit('editTask', taskData)
		},
		completeTask (context, id) {
			for(let i in context.state.tasks) {
				if(id === context.state.tasks[i].id) context.commit('completeTask', i)
			}
			
		},
		deleteTask (context, id) {
			for(let i in context.state.tasks) {
				if(id === context.state.tasks[i].id) context.commit('deleteTask', i)
			}
			
		},
		registerUser(context, data) {
			return new Promise((resolve, reject)=>{
				context.commit('registerUser', data);
				resolve(true)
			})
		},
		authorizeUser(context, data) {
			return new Promise((resolve, reject)=>{
				let user = false;
				for(let i in context.state.users) {
					if(context.state.users[i].email === data.email && context.state.users[i].password === data.password) {
						user = context.state.users[i]
					}
				}
				if(user) {
					context.commit('authorizeUser', user);
					resolve(true);
				} else {
					reject(false)
				}
			})
			
		},
	}
})
export default store;