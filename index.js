import { createApp  } from 'vue'
import App from './src/App.vue'
import createState  from './src/store/vue3Store.js'
const app = createApp(App);
app.provide(createState());
app.mount('#app')
